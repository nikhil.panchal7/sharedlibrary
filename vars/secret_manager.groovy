#!/usr/bin/env groovy

def  call(SECRET_ID, REGION, SECRET_FILE_LOCATION){

sh "aws secretsmanager get-secret-value --secret-id ${SECRET_ID} --region ${REGION} | grep -i SecretString | awk '{print \$2}' | tr -d '{}' | tr -d '\"' | tr ',' ' ' | tr -d '\\' 2>/dev/null > ./check.txt "

sh """no_field=`cat ./check.txt | awk '{ print NF }'`
cat ./check.txt | tr ';' ',' |awk '{ for (line = 1; line <= '\${no_field}'; line++) print \$line }' > ./check2.txt 
cat ./check2.txt | sed 's/:/=/1'> "${SECRET_FILE_LOCATION}"
"""
}
